package h.mersad.rubicon.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Mersad on 10/4/2018.
 */

public class Movie extends RealmObject {

    @PrimaryKey
    public int id;
    public String overview;
    public String poster_path;
    public double popularity;
    public String title;
}

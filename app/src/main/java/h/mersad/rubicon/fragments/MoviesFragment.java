package h.mersad.rubicon.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import h.mersad.rubicon.R;
import h.mersad.rubicon.adapters.MoviesAdapter;
import h.mersad.rubicon.app.API;
import h.mersad.rubicon.app.AppController;
import h.mersad.rubicon.model.Movie;
import io.realm.Case;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MoviesFragment extends Fragment implements TextWatcher {

    Activity activity;
    MoviesAdapter moviesAdapter;
    List<Movie> items = new ArrayList<>();
    EditText searchBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchBar = view.findViewById(R.id.searchBar);
        searchBar.addTextChangedListener(this);

        RecyclerView recyclerView = view.findViewById(R.id.photosView);
        moviesAdapter = new MoviesAdapter(items, getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(moviesAdapter);

        AppController.getInstance().api.getMovies().enqueue(new Callback<API.GetMoviesResponse>() {
            @Override
            public void onResponse(Call<API.GetMoviesResponse> call, Response<API.GetMoviesResponse> response) {
                final API.GetMoviesResponse body = response.body();
                if (body != null)
                    try (Realm realm = Realm.getDefaultInstance()) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.where(Movie.class).findAll().deleteAllFromRealm();
                                realm.insertOrUpdate(body.results);
                            }
                        });

                        updateViews();
                    }
            }

            @Override
            public void onFailure(Call<API.GetMoviesResponse> call, Throwable t) {
                Log.d("tag", "errrr");
            }
        });

        updateViews();
    }


    public void updateViews() {
        try (Realm realm = Realm.getDefaultInstance()) {
            items.clear();
            items.addAll(realm.copyFromRealm(realm.where(Movie.class).findAll()));
            moviesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        if (charSequence.toString().isEmpty() || charSequence.toString().length() < 3)
            updateViews();
        else
            try (Realm realm = Realm.getDefaultInstance()) {
                items.clear();
                items.addAll(realm.copyFromRealm(realm.where(Movie.class).like("title", "*" + charSequence.toString() + "*", Case.INSENSITIVE).findAll()));
                moviesAdapter.notifyDataSetChanged();
            }

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
package h.mersad.rubicon.app;

import java.util.List;

import h.mersad.rubicon.model.Movie;
import h.mersad.rubicon.model.TvShows;
import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by Mersad on 9/29/2018.
 */

public interface API {

    class GetMoviesResponse {
        public int page;
        public int total_results;
        public int total_pages;
        public List<Movie> results;
    }

    class GetTvShowsResponse {
        public int page;
        public int total_results;
        public int total_pages;
        public List<TvShows> results;
    }

    @POST("movie/top_rated?api_key=d13255250c9f11fb498626a84c5b3655&language=en")
    Call<GetMoviesResponse> getMovies();

    @POST("tv/popular?api_key=d13255250c9f11fb498626a84c5b3655&language=en")
    Call<GetTvShowsResponse> getTvShows();


}

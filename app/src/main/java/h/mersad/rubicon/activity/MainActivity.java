package h.mersad.rubicon.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;
import java.util.List;

import h.mersad.rubicon.R;
import h.mersad.rubicon.adapters.MoviesAdapter;
import h.mersad.rubicon.fragments.HomeFragment;
import h.mersad.rubicon.model.Movie;

public class MainActivity extends AppCompatActivity {
    private HomeFragment homeFragment;
    private FragmentManager fragmentManager;
    BottomNavigationViewEx navigation;
    MoviesAdapter moviesAdapter;
    List<Movie> items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigation = findViewById(R.id.bottomView);
        navigation.setTextVisibility(false);
        navigation.enableAnimation(false);
        navigation.enableShiftingMode(false);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setItemIconTintList(null);

        homeFragment = new HomeFragment();
        homeFragment.activity = this;
        moviesAdapter = new MoviesAdapter(items, this);

        fragmentManager = getSupportFragmentManager();
        replaceFragment(homeFragment);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    replaceFragment(homeFragment);
                    return true;
            }
            return false;
        }
    };

    private void replaceFragment(Fragment newFragment) {
        fragmentManager.beginTransaction().replace(R.id.fragment_container, newFragment).commitAllowingStateLoss();
    }
}

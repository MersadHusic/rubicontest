package h.mersad.rubicon.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import h.mersad.rubicon.R;

public class MovieActivity extends AppCompatActivity {
    public TextView tittle, description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        tittle = findViewById(R.id.title_overview);
        description = findViewById(R.id.overview_overview);
        Glide.with(this).load(getIntent().getStringExtra("link")).into((ImageView) findViewById(R.id.imageView));
        tittle.setText(getIntent().getStringExtra("title"));
        description.setText(getIntent().getStringExtra("description"));
    }
}

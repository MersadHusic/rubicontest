package h.mersad.rubicon.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import h.mersad.rubicon.R;
import h.mersad.rubicon.activity.MovieActivity;
import h.mersad.rubicon.model.TvShows;

/**
 * Created by Mersad on 10/11/2018.
 */

public class TvShowAdapter extends RecyclerView.Adapter<TvShowAdapter.PhotoViewHolder> {

    List<TvShows> items = new ArrayList<>();
    Activity activity;

    public TvShowAdapter(List<TvShows> items, Activity activity) {
        this.items = items;
        this.activity = activity;
    }

    @Override
    public TvShowAdapter.PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
        return new TvShowAdapter.PhotoViewHolder(v);
    }


    @Override
    public void onBindViewHolder(TvShowAdapter.PhotoViewHolder holder, int position) {
        final TvShows item = items.get(position);

        final String link = "http://image.tmdb.org/t/p/w185/" + item.poster_path;
        holder.positionFilms.setText(item.popularity + "");
        holder.titleView.setText(item.name);
        holder.description.setText(item.overview);

        Glide.with(activity).load(link).into(holder.photoView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MovieActivity.class);
                intent.putExtra("link", link);
                intent.putExtra("title", item.name);
                intent.putExtra("description", item.overview);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder {
        ImageView photoView;
        TextView positionFilms, titleView, description;

        PhotoViewHolder(View itemView) {
            super(itemView);
            photoView = itemView.findViewById(R.id.imageView);
            positionFilms = itemView.findViewById(R.id.positionFilm);
            titleView = itemView.findViewById(R.id.imageTitle);
            description = itemView.findViewById(R.id.description);
        }
    }
}